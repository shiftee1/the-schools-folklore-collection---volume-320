This project formats a Volume of The School's Collection as a LaTex Document.

The Schools Collection is a compilation of Irish folklore from the 1940s. 

The stories were collected by schoolchildren across the country under the 
supervision of the Irish Folklore Commission.

The collection is available from duchas.ie under the terms of the Creative 
Commons Attribution-NonCommercial 4.0 International License.
